from pymongo import MongoClient
from environment import Environment


class Conection():
    def __init__(self):
        env = Environment()
        config = env.db()

        client = MongoClient(config['HOST'], config['PORT'])
        agenda = client.agenda
        self.contactos = agenda.contactos
