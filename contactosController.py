from flask import request
from bson.json_util import dumps
from conection import Conection
from bson.objectid import ObjectId

conection = Conection()
campos = ["nombres", "apellidoPaterno",
          "apellidoMaterno", "movil", "fijo", "email"]


class ContactosController:

    def validarContacto(self, contacto):
        if all(campo in campos for campo in contacto):
            return contacto
        else:
            return None

    def anadir(self):
        contacto = self.validarContacto(request.get_json(force=True))
        if contacto is None:
            return 'Los campos ingresados no son correctos'
        else:
            conection.contactos.insert_one(contacto)
            return "Contacto anadido"

    def listar(self):
        resultado = dumps(conection.contactos.find())
        return resultado

    def mostrar(self, id):
        resultado = dumps(conection.contactos.find({"_id": ObjectId(id)}))
        return resultado

    def actualizar(self, id):
        contacto = self.validarContacto(request.get_json(force=True))
        if contacto is None:
            return 'Los campos ingresados no son correctos'
        else:
            conection.contactos.update_one(
                {"_id": ObjectId(id)}, {"$set": contacto})
            return "Contacto actualizado"

    def eliminar(self, id):
        conection.contactos.delete_one({"_id": ObjectId(id)})
        return "Contacto eliminado"
