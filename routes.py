from flask import Blueprint

from contactosController import ContactosController

contactosController = ContactosController()

bP = Blueprint('bP', __name__)


@bP.route('/contactos/list')
def listarContactos():
    return contactosController.listar()


@bP.route('/contacto', methods=['POST'])
def anadirContacto():
    return contactosController.anadir()


@bP.route('/contacto/<id>', methods=['GET'])
def mostrarContacto(id):
    return contactosController.mostrar(id)


@bP.route('/contacto/<id>', methods=['DELETE'])
def eliminarContacto(id):
    return contactosController.eliminar(id)


@bP.route('/contacto/<id>', methods=['PUT'])
def actualizarContacto(id):
    return contactosController.actualizar(id)
